var express = require('express');
var bcrypt = require('bcrypt');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userSchema = new Schema({
  local: {
    email: { type: String, unique: true },
    password: { type: String }
  },
  name: { type: String },
  c2wCoin: { type: Number, default: 100 },
  roles: {
    type: String,
    default: 'User',
    enum: ['User', 'Admin', 'Owner', 'It']
  },
  profileDisplay: { type: Boolean, default: false },
  facebook: {
    email: { type: String, unique: true },
    id: { type: String },
    token: { type: String }
  },
  google: {
    email: { type: String, unique: true },
    id: { type: String },
    token: { type: String }
  }
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

userSchema.methods.generateHash = function(password){
	return bcrypt.hashSync(password, bcrypt.genSaltSync(9));
}

userSchema.methods.validPassword = function(password){
	return bcrypt.compareSync(password, this.local.password);
}

var User = mongoose.model('User', userSchema);

module.exports = User;
