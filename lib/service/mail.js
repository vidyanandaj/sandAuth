var express = require('express');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');

var transporter = nodemailer.createTransport({
  host: 'mail.google.com',
  port: 465,
  secure: true,
  auth: {
    user: 'example@domain.com',
    pass: 'password'
  }
});

module.exports = transporter;
