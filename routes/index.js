var express = require('express');
var passport = require('passport');
var router = express.Router();

var mailHelper = require('../lib/service/mail');

function isLoggedIn(req, res, next) {
  if(req.isAuthenticated()){
		return next();
	}
  res.redirect('/login');
}

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Sand01' });
});

router.get('/login', function(req, res, next){
  res.render('login', { message: req.flash('loginMessage') });
});

router.post('/login', passport.authenticate('local-login', {
		successRedirect: '/profile',
		failureRedirect: '/login',
		failureFlash: true
}));

router.get('/signup', function(req, res, next){
		res.render('signup', { message: req.flash('signupMessage') });
});

// router.post('/signup', passport.authenticate('local-signup', {
// 		successRedirect: '/thankYou',
// 		failureRedirect: '/signup',
// 		failureFlash: true
// }));

router.post('/signup', passport.authenticate('local-signup', { failureRedirect: '/signup', failureFlash: true }), function(req, res, next) {
  var mailOptions = {
    from: '"Vidya" <vidyajaji@gmail.com>', // sender address
    to: req.user.local.email, // list of receivers
    subject: 'Hello ✔', // Subject line
    text: 'Hello world ?', // plain text body
    html: '<b>Hello world ?</b>' // html body
  };
  mailHelper.sendMail(mailOptions, (error, info) => {
    if (error) {
        return next(error);
    }
    console.log('Message %s sent: %s', info.messageId, info.response);
    res.redirect('/thankYou');
  });
})

router.get('/thankYou', function(req, res, next){
		res.render('thankYou', { title: 'Sand01', message: req.flash('signupMessage') });
});

router.get('/profile', isLoggedIn, function(req, res, next){
    switch (req.user.roles) {
      case 'User':
        res.render('profileUser', { user: req.user });
        break;
      case 'Admin':
        res.render('profileAdmin', { user: req.user });
        break;
      case 'Owner':
        res.render('profileOwner', { user: req.user });
        break;
      case 'It':
        res.render('profileIt', { user: req.user });
        break;
      default:
        res.render('profile', { user: req.user });
        break;
    }
});

router.get('/auth/facebook', passport.authenticate('facebook', {scope: ['email']}));
router.get('/auth/facebook/callback', passport.authenticate('facebook', { successRedirect: '/profile', failureRedirect: '/' }));
router.get('/auth/google', passport.authenticate('google', {scope: ['profile', 'email']}));
router.get('/auth/google/callback', passport.authenticate('google', { successRedirect: '/profile', failureRedirect: '/' }));

router.get('/connect/facebook', passport.authorize('facebook', { scope: 'email' }), function(req, res, next){
		console.log("account" + req.account);
});
router.get('/connect/google', passport.authorize('google', { scope: ['profile', 'email'] }));
router.get('/connect/local', function(req, res, next){
		res.render('connect-local', { message: req.flash('signupMessage')});
});
router.post('/connect/local', passport.authenticate('local-signup', {
		successRedirect: '/profile',
		failureRedirect: '/connect/local',
		failureFlash: true
}));

router.get('/unlink/facebook', function(req, res){
		var user = req.user;

		user.facebook.token = null;

		user.save(function(err){
			if(err)
				throw err;
			res.redirect('/profile');
		});
	});
router.get('/unlink/local', function(req, res){
		var user = req.user;

		user.local.username = null;
		user.local.password = null;

		user.save(function(err){
			if(err)
				throw err;
			res.redirect('/profile');
		});
});
router.get('/unlink/google', function(req, res){
		var user = req.user;
		user.google.token = null;

		user.save(function(err){
			if(err)
				throw err;
			res.redirect('/profile');
		});
});

router.get('/logout', function(req, res, next){
		req.logout();
		res.redirect('/');
});

module.exports = router;
